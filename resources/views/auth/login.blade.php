<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISC</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <img id="TNM" src="src/TNM.png" class="img-fluid rounded float-left" alt="Responsive image">
                </div>
                <div class="col-4 text-center">
                    <img id="ISC" src="src/Itmorelia.png" class="img-fluid" alt="Responsive image">
                </div>
                <div class="col-4">
                    <img id="TEC" src="src/Itmorelia2.png" class="img-fluid rounded float-right" alt="Responsive image">
                </div>
            </div>
        </div>
        <!-- Formulario -->
        <div class="formulario">
            <div class="text-center">


                <form method="POST" action="{{ route('login') }}" class="form-signin">
                    {{ csrf_field() }}

                    <img class="mb-4" src="src/_sistemas.jpg" alt="" width="72" height="72">

                    <h1 class="h3 mb-3 font-weight-normal">Iniciar sesión</h1>

                    <label for="email" class="sr-only">e-mail</label>
                    <input type="email" id="email" class="form-control" placeholder="email" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif

                    <label for="password" class="sr-only">Password</label>
                    <input type="password" id="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif

                    <div class="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me"> Recuérdame
                        </label>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary btn-block">
                        Login
                    </button>

                    {{-- <a href="" class="btn btn-lg btn-primary btn-block" onclick="next()" type="submit">login</a> --}}




                </form>
            </div>
        </div>
    </div>
</body>
</html>
