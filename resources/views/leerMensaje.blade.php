@extends('layout');

@section('content')

<a href="{{route('mensaje.edit', $mensaje)}}">editar</a><br/><br/>

Enviado por: {{ $mensaje->id_remitente }}<br />
Prioridad: {{ $mensaje->prioridad }}<br />
Asunto: {{ $mensaje->asunto }} <br />
Contenido {{ $mensaje->contenido }}<br />
Fecha: {{ $mensaje->created_at->format('y/m/d') }}<br />
Adjunto: {{ $mensaje->adjunto }}<br />

@endsection

@section('title', 'About')
