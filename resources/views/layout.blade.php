<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title>ICS - @yield('title')</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{asset('css/crear_comunicado.css')}}" rel="stylesheet">
    <link href="{{asset('css/mail.css')}}" rel="stylesheet">

    <link href="{{asset('css/logos.css')}}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<header>
@include('partials/nav')
</header>
<div class="container">
    <div class="row">
        @include('partials/aside')

        @yield('content')
    </div>
</div>
</body>
</html>
