@extends('layout');

@section('title', 'Enviar Mensaje')

@section('content')



<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body message">
            <p class="text-center">Nuevo Mensaje</p>
            <form class="form-horizontal" role="form" method="POST" action="{{ route ('mensaje.store') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="destinatario" class="col-sm-1 control-label">Para:</label>
                    <div class="col-sm-11">
                        <select name="destinatario" class="form-control select2-offscreen" id="destinatario">
                            @forelse($users as $userItem)
                                <option value = "{{$userItem->id}}">{{$userItem->apellido}} {{$userItem->nombre}}</option>
                            @empty

                            @endforelse
                        </select>
                        @if ($errors->has('destinatario'))
                        <span class="help-block">
                            <strong>{{ $errors->first('destinatario') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="prioridad" class="col-sm-1 control-label">prioridad:</label>
                    <div class="col-sm-11">
                        <select name="prioridad" class="form-control select2-offscreen" id="prioridad">
                            <option value="1">Social</option>
                            <option value="2">No Urgente</option>
                            <option value="3" selected>Urgente</option>
                            <option value="4">Alta prioridad</option>

                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="asunto" class="col-sm-1 control-label">asunto:</label>
                    <div class="col-sm-11">
                        <input name="asunto" class="form-control select2-offscreen" id="asunto" placeholder="asunto" value="{{ old('asunto') }}">
                        @if ($errors->has('asunto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asunto') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>




                {{-- <div class="col-sm-11 col-sm-offset-1">

                    <div class="btn-toolbar" role="toolbar">

                        <div class="btn-group">
                            <button class="btn btn-default"><span class="fa fa-bold"></span></button>
                            <button class="btn btn-default"><span class="fa fa-italic"></span></button>
                            <button class="btn btn-default"><span class="fa fa-underline"></span></button>
                        </div>

                        <div class="btn-group">
                            <button class="btn btn-default"><span class="fa fa-align-left"></span></button>
                            <button class="btn btn-default"><span class="fa fa-align-right"></span></button>
                            <button class="btn btn-default"><span class="fa fa-align-center"></span></button>
                            <button class="btn btn-default"><span class="fa fa-align-justify"></span></button>
                        </div>

                        <div class="btn-group">
                            <button class="btn btn-default"><span class="fa fa-indent"></span></button>
                            <button class="btn btn-default"><span class="fa fa-outdent"></span></button>
                        </div>

                        <div class="btn-group">
                            <button class="btn btn-default"><span class="fa fa-list-ul"></span></button>
                            <button class="btn btn-default"><span class="fa fa-list-ol"></span></button>
                        </div>
                        <button class="btn btn-default"><span class="fa fa-trash-o"></span></button>
                        <button class="btn btn-default"><span class="fa fa-paperclip"></span></button>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-tags"></span> <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="label label-danger"> Prioridad alta</span></a></li>
                                <li><a href="#"> <span class="label label-info">Urgente</span></a></li>
                                <li><a href="#"> <span class="label label-success">No urgente</span></a></li>
                                <li><a href="#"> <span class="label label-warning">Social</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <br> --}}

                <div class="form-group">
                    <textarea class="form-control" id="contenido" name="contenido" rows="12" placeholder="Escribe tu mensaje">{{ old('contenido') }}</textarea>
                    @if ($errors->has('contenido'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contenido') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!--/.col-->


@endsection
