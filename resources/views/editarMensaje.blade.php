@extends('layout');

{{-- @section('title', 'Editar mensaje') --}}

@section('content')
Editar mensaje<br /><br />



<form method="POST" action="{{ route ('mensaje.update', $mensaje) }} ">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    Tu ID: <input name="id_remitente" placeholder="Ingresa tu ID" value="{{ old('id_remitente', $mensaje->id_remitente) }}" /><br />
    {!! $errors->first('id_remitente','<small>:message:</small><br />') !!}
    Prioridad: <select name="prioridad">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3" selected>3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select><br />
    Asunto: <input name="asunto" placeholder="Ingrese asunto" value="{{ old('asunto',$mensaje->asunto) }}" /><br />
    {!! $errors->first('asunto','<small>:message</small><br />') !!}
    Contenido:<br />
    <textarea name="contenido" placeholder="Ingrese contenido">{{ old('contenido',$mensaje->contenido) }}</textarea><br />
    {!! $errors->first('contenido','<small>:message</small><br />') !!}
    <button>Actualizar</button>
</form>
@endsection
