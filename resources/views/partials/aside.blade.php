<div class="col-sm-3">
    <a href="{{route('mensaje.create')}}" class="btn btn-danger btn-block btn-compose-email">Crear Comunicado</a>
    <ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
        <li class="active">
            <a href="{{route('mensaje.outbox')}}">
                <i class="fa fa-inbox"></i> Recibidos
                {{-- <span class="badge pull-right">8</span> --}}
            </a>
        </li>
        <li>
            <a href="{{route('mensaje.outbox')}}">
                <i class="fa fa-envelope-o"></i> Enviados
                {{-- <span class="badge label-info pull-right inbox-notification">5</span> --}}
            </a>
        </li>
        {{-- <li>
                    <a href="#">
                        <i class="fa fa-file-text-o"></i> Borradores
                        <span class="badge label-info pull-right inbox-notification">3</span>
                    </a>
                </li> --}}
        <li>
            <a href="{{route('register')}}">
                <i class="fa fa-file-text-o">

                </i> Registrar usuarios
            </a>
        </li>
    </ul>

    <!-- /.nav -->

    <h5 class="nav-email-subtitle">Prioridades</h5>
    <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow">
        <li>
            <a href="#">
                <i class="fa fa-folder-open"></i> Prioridad Alta
                {{-- <span class="label label-danger pull-right">3</span> --}}
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-folder-open"></i> Urgente
                {{-- <span class="label label-warning pull-right">0</span> --}}
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-folder-open"></i> No Urgente
                {{-- <span class="label label-success pull-right">0</span> --}}
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-folder-open"></i> Social
                {{-- <span class="label label-info pull-right">0</span> --}}
            </a>
        </li>
    </ul><!-- /.nav -->
</div>
