{{-- <nav>
    <ul>
        @auth
        Hola, {{ auth()->user()->nombre }} {{ auth()->user()->apellido }}

<li>Home</li>
<li><a href="{{ route('mensaje.outbox') }}">Bandeja de entrada</a></li>
<li><a href="{{ route('mensaje.create') }}">Nuevo mensaje</a></li>
<li><a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        Cerrar Sesión
    </a></li>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
@else
<li><a href="{{ route('login') }}">Login</a></li>
@endauth
</ul>
</nav> --}}

<nav class="navbar  navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ICS</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav navbar-right">
            <li><a>{{ auth()->user()->nombre }} {{ auth()->user()->apellido }}</a></li>
            <li>
                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Cerrar Sesión
                </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            {{-- <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Perfil <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Cambiar Contraseña</a></li>
                    <li><a href="#">Cambiar Foto Perfil</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="salir">Cerrar Sesión</a></li>
                </ul>
            </li> --}}
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
