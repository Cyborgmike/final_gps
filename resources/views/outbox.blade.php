@extends('layout')

@section('content')
{{-- Bandeja de Salida <br />

<ul>
    @forelse($mensajes as $mensajeItem)
    <li><a href="{{ route('mensaje.show',$mensajeItem) }}">{{ $mensajeItem->asunto }}</a></li>
@empty
<li>no hay mensajes</li>
@endforelse
</ul>
<br /> --}}


<div class="col-sm-9">
    <div class="panel rounded shadow panel-teal">
        <div class="panel-heading">
            <div class="pull-left">
                <h3 class="panel-title">Bandeja de Salida</h3>
            </div>
            {{-- <div class="pull-right">
                <form action="#" class="form-horizontal mr-5 mt-3">
                    <div class="form-group no-margin no-padding has-feedback">
                        <input type="text" class="form-control no-border" placeholder="Buscar">
                        <button type="submit" class="btn btn-theme fa fa-search form-control-feedback"></button>
                    </div>
                </form>
            </div> --}}
            <div class="clearfix"></div>
        </div>
        <!-- /.panel-heading -->
        {{-- <div class="panel-sub-heading inner-all">
            <div class="pull-left">
                <ul class="list-inline no-margin">
                    <li>
                        <div class="ckbox ckbox-theme">
                            <input id="checkbox-group" type="checkbox" class="mail-group-checkbox">
                            <label for="checkbox-group"></label>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                All <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">None</a></li>
                                <li><a href="#">Read</a></li>
                                <li><a href="#">Unread</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="" data-original-title="Archive"><i class="fa fa-inbox"></i></button>
                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="" data-original-title="Report Spam"><i class="fa fa-warning"></i></button>
                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </li>
                    <li class="hidden-xs">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm">More</button>
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#"><i class="fa fa-edit"></i> Mark as read</a></li>
                                <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <ul class="list-inline no-margin">
                    <li class="hidden-xs"><span class="text-muted">Showing 1-50 of 2,051 messages</span></li>
                    <li>
                        <div class="btn-group">
                            <a href="#" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                            <a href="#" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </li>
                    <li class="hidden-xs">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog"></i> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li class="dropdown-header">Display density :</li>
                                <li class="active"><a href="#">Comfortable</a></li>
                                <li><a href="#">Cozy</a></li>
                                <li><a href="#">Compact</a></li>
                                <li class="dropdown-header">Configure inbox</li>
                                <li><a href="#">Settings</a></li>
                                <li><a href="#">Themes</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div><!-- /.panel-sub-heading --> --}}
        <div class="panel-body no-padding">

            <div class="table-responsive">
                <table class="table table-hover table-email">
                    <tbody>
                        @forelse($mensajes as $mensajeItem)
                        <tr>
                            {{-- <td>
                                    <div class="ckbox ckbox-theme">
                                    <input id="checkbox1" type="checkbox" checked="checked" class="mail-checkbox">
                                    <label for="checkbox1"></label>
                                    <span class="label label-success">Social</span>
                                </div>

                            </td> --}}

                            <td>

                                <div class="media">
                                    {{-- <a href="vistaMail" class="pull-left">
                                        <img alt="..." src="https://bootdey.com/img/Content/avatar/avatar1.png" class="media-object">
                                    </a> --}}

                                    <div class="media-body">
                                        <h4 class="rext-primary">Nombre de remitente</h4>
                                        <strong>
                                            <a href="{{ route('mensaje.show',$mensajeItem) }}">{{ $mensajeItem->asunto }}</a>
                                        </strong>
                                        {{-- <span class="label label-success">New</span> --}}
                                        <span class="media-meta">
                                            {{ $mensajeItem->created_at->format('Y/m/d') }}<br />
                                            @if($mensajeItem->prioridad == 1)
                                            <span class="label label-info">Social</span>
                                            @elseif($mensajeItem->prioridad == 2)
                                            <span class="label label-success">No urgente</span>
                                            @elseif($mensajeItem->prioridad == 3)
                                            <span class="label label-warning">Urgente</span>
                                            @else
                                            <span class="label label-danger">Alta Prioridad</span>
                                            @endif

                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @empty

                        @endforelse

                        {{-- <tr>
                            <td>
                                <div class="ckbox ckbox-theme">
                                    <input id="checkbox2" type="checkbox" class="mail-checkbox">
                                    <label for="checkbox2"></label>
                                </div>
                            </td>
                            <td>
                                <div class="media">
                                    <a href="#" class="pull-left">
                                        <img alt="..." src="https://bootdey.com/img/Content/avatar/avatar2.png" class="media-object">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="text-primary">Jennifer Poiyem</h4>
                                        <p class="email-summary"><strong>Send you gift</strong> Sed do eiusmod tempor incididunt...<span class="label label-success">New</span> </p>
                                        <span class="media-meta">Today at 1:13am</span>
                                        <span class="media-attach"><i class="fa fa-paperclip"></i></span>
                                    </div>
                                </div>
                            </td>
                        </tr> --}}
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->

        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div>



@endsection

@section('title', 'Bandeja de Salida')
