<?php

namespace App\Http\Controllers;

use App\Mensaje;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MensajeRequest;
use App\Mail\MailNotification;
use Illuminate\Support\Facades\Mail;

class MensajeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function outbox()
    {
        //
        $mensajes = Mensaje::latest()->get();

        return view('outbox',compact('mensajes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::get();
        return view('crearMensaje',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        request()->validate([
            'asunto' => 'required',
            'prioridad' => 'required',
            'contenido' => 'required',
        ]);

        Mensaje::create([
            'asunto' => request('asunto'),
            'prioridad' => request('prioridad'),
            'contenido' => request('contenido'),
            'id_remitente' => auth()->user()->id
        ]);

        // Mail::to('dajaornu@gmail.com')->queue(new MailNotification);
        return redirect()->route('mensaje.outbox');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mensaje $mensaje)
    {
        //

        return view('leerMensaje',compact('mensaje'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mensaje $mensaje)
    {
        //
        return view('editarMensaje',compact('mensaje'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Mensaje $mensaje, MensajeRequest $request)
    {
        //
        $mensaje->update( $request->validated() );
        return redirect()->route('mensaje.show', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
