<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

/*
ejemplos de rutas
Route::post()
Route::put()
Route::patch()
Route::delete()
*/

//function = Closure (lo que verá el usuario)
//Al entrar a prueba.test
/*

Route::get('/', function(){
    return "hola mundo";
});

//al entrar a prueba.test/contacto
Route::get('subseccion', function(){
    return "soy una subseccion";
});

//Pasando paramétros vía URL obligatorio
Route::get('saludo/{nombre}', function($nombre){
    return "Saludos ". $nombre;
});

//Pasando paramétros vía URL opcional
Route::get('saludoopcional/{nombre?}', function($nombre ="invitado"){
    return "Saludos ". $nombre;
});

//RUTAS CON NOMBRE
//creamos ruta con el nombre faq, si se cambia la URL, cambiar solo el parámetro faq
Route::get('pregunta', function(){
    return "pregúuuuuntame";
})->name('faq');

//seccion con 5 enlaces a faq, referenciadas por el nombre de la ruta, si la URL cambia se actualizan todos los enlaces
Route::get('namedroutes',function(){
    echo "<a href='/faq'> Preguntas 1</a><br>";
    echo "<a href='".route('faq')."'> Preguntas 2</a><br>";
    echo "<a href='".route('faq')."'> Preguntas 3</a><br>";
    echo "<a href='".route('faq')."'> Preguntas 4</a><br>";
    echo "<a href='".route('faq')."'> Preguntas 5</a><br>";
});

Route::get('vista', function(){
    return view('vista');
})->name('vista');

//uso de layouts de navegación con ejemplo alternativo para declarar rutas (recomendado retornar vistas con poca o nula información)
Route::view('homelayout', 'homelayout')->name('homelayout');
Route::view('aboutlayout', 'aboutlayout')->name('aboutlayout');
Route::view('contactlayout', 'contactlayout')->name('contactlayout');
*/

Route::get('/outbox', 'MensajeController@outbox')->name('mensaje.outbox');
Route::get('/crearMensajes', 'MensajeController@create')->name('mensaje.create');
Route::post('crearMensajes', 'MensajeController@store')->name('mensaje.store');
Route::get('leerMensaje/{mensaje}','MensajeController@show')->name('mensaje.show');
Route::get('editarMensaje/{mensaje}','MensajeController@edit')->name('mensaje.edit');
Route::patch('Mensaje/{mensaje}','MensajeController@update')->name('mensaje.update');

// Auth::routes();
//las siguientes 10 rutas equivalen a lo mismo que el comando de arriba, favor de no borrar
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/reset','Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register','Auth\RegisterController@register');

Route::get('/home', 'MensajeController@outbox')->name('home');
// Route::get('/', 'HomeController@index');